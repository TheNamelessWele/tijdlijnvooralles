using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class popupgen : MonoBehaviour
{
    public GameObject popupPrefab;
    // Start is called before the first frame update
    public void GeneratePopup()
    {
        GameObject popup = Instantiate(popupPrefab);
        popup.transform.parent = this.gameObject.transform.parent.transform;
        
        Destroy(popup, 3f);
    }
}
