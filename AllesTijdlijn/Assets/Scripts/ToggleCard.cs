using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToggleCard : MonoBehaviour
{
    private GameObject originalParent;
    private TimelineScript tlScript;
    // Start is called before the first frame update
    void Start()
    {
    }

    public void Toggle()
    {
        GameObject newCard = this.gameObject;
        newCard.transform.parent = GameObject.Find("SecondContainer").transform;
        newCard.SetActive(!this.gameObject.activeSelf);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
