using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class TagSelection : MonoBehaviour
{
    public static HashSet<string> selectedTags = new HashSet<string>();
    private TMP_Text tagname;
    private bool isSelected;

    // Start is called before the first frame update
    void Start()
    {
        isSelected = false;
        tagname = GetComponent<TMP_Text>();
    }

    public void ToggleTag()
    {
        if (isSelected) SearchAndSort.selectedTags.Remove(tagname.text);
        else SearchAndSort.selectedTags.Add(tagname.text);
        isSelected = !isSelected;
    }
}
