using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragDrop : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IEndDragHandler, IDragHandler, IDropHandler
{
    [SerializeField] private Canvas canvas;

    private RectTransform rectTransform;
    private Vector2 realPos;
    private Transform realParent;
    private CanvasGroup canvasGroup;
    public bool isSuccesfuldDrop;

    public int startDate;
    public int endDate;
    public int timelineStart = 1800;
    public int timelineEnd = 2000;

    private void Awake()
    {
        rectTransform = GetComponent<RectTransform>();
        canvasGroup = GetComponent<CanvasGroup>();
        canvas = GameObject.FindGameObjectWithTag("MainCanvas").GetComponent<Canvas>();
    }


    public void OnBeginDrag(PointerEventData eventData)
    {
        Debug.Log("BeginDrag");
        realParent = transform.transform;
        realPos = rectTransform.anchoredPosition;
        rectTransform.parent = GameObject.Find("WhileDragging").transform;
        canvasGroup.blocksRaycasts = false;
        isSuccesfuldDrop = false;
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnDrag(PointerEventData eventData)
    {
        Debug.Log("OnDrag");
        //update position of the object
        rectTransform.anchoredPosition += eventData.delta / canvas.scaleFactor;
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        Debug.Log("OnEndDrag");
        if (!isSuccesfuldDrop)
        {
            rectTransform.anchoredPosition = realPos;
            rectTransform.SetParent(realParent, false);
        }
        canvasGroup.blocksRaycasts = true;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        Debug.Log("OnPointerDrag");
        //throw new System.NotImplementedException();
    }

    public void OnDrop(PointerEventData eventData)
    {
        Debug.Log("OnDrop");
    }

    public void SetObject()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);

        float start = (startDate - timelineStart) * 7.5f;
        float end = (endDate-startDate) * 7.5f;

        rectTransform.localPosition = new Vector3(start-750, 0);
        rectTransform.sizeDelta = new Vector2(end,rectTransform.sizeDelta.y);
    }
}
