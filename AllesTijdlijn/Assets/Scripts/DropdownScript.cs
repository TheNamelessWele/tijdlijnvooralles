using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DropdownScript : MonoBehaviour
{
    //depending on which button is pressed, a different second dropdown menu is set active
    public TMPro.TMP_Dropdown mainDrop;
    public TMPro.TMP_Dropdown subDrop1;
    public TMPro.TMP_Dropdown subDrop2;
    public TMPro.TMP_Dropdown subDrop3;

    public TMPro.TMP_Dropdown[] subMenus;

    public GameObject SubDropdownCollection;

    public void OpSelect()
    {
        //GameObject[] menuChildren = SubDropdownCollection.GetComponentsInChildren<GameObject>();
        //foreach (GameObject item in menuChildren)
        //{
        //    item.SetActive(false);
        //}

        if(mainDrop.value == 0)
        {
            //do nothing
        }
        else if (mainDrop.value == 1)
        {
            subDrop1.gameObject.SetActive(true);
        }
        else if (mainDrop.value == 2)
        {
            subDrop2.gameObject.SetActive(true);
        }
        else if (mainDrop.value == 3)
        {
            subDrop3.gameObject.SetActive(true);
        }
    }

}
