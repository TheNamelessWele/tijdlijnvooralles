using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class EmptyTimeline : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void EmptyAndMakeNew()
    {  
        int j = 0;
        string gameDataPath = Application.streamingAssetsPath + "/gamedata.tsv";
        string path = Application.streamingAssetsPath + "/backupGamedata" + j + ".tsv";
       
        while (File.Exists(path))
        {
            j++;
            path = Application.streamingAssetsPath + "/backupGamedata" + j + ".tsv";
        }

        File.Move(Application.streamingAssetsPath + "/gamedata.tsv", path);
        using(StreamWriter sw = File.CreateText(gameDataPath))
        {
            sw.WriteLine("\tStart\tEnd\tField(s)\tInfo\tCountry\tStartdate sure\tEnddate sure\tLink");
        }
        
    }
}
