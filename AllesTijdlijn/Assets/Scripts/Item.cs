using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    public string objName;
    public Date startDate;
    public Date endDate;
    public List<string> tags;
    public string information;
    public List<string> country;
    public bool startDateSure;
    public bool endDateSure;
    public string link;
    public int width = 10;
    public int x = 0;

    public int timelineStart = 1800;
    public int timelineEnd = 2000;
    public Item(string objName, Date startDate, Date endDate, List<string> tags, string information, List<string> country, bool startDateSure, bool endDateSure, string link)
    {
        this.objName = objName;
        this.startDate = startDate;
        this.endDate = endDate;
        this.tags = tags;
        this.information = information;
        this.country = country;
        this.startDateSure = startDateSure;
        this.endDateSure = endDateSure;
        this.link = link;
    }

    public override bool Equals(object obj)
    {
        return obj is Item item &&
               base.Equals(obj) &&
               name == item.name &&
               EqualityComparer<Date>.Default.Equals(startDate, item.startDate) &&
               EqualityComparer<Date>.Default.Equals(endDate, item.endDate);
    }

    public override int GetHashCode()
    {
        int hashCode = -827392131;
        hashCode = hashCode * -1521134295 + base.GetHashCode();
        hashCode = hashCode * -1521134295 + EqualityComparer<string>.Default.GetHashCode(name);
        hashCode = hashCode * -1521134295 + EqualityComparer<Date>.Default.GetHashCode(startDate);
        hashCode = hashCode * -1521134295 + EqualityComparer<Date>.Default.GetHashCode(endDate);
        return hashCode;
    }

    public void SetObject()
    {
        RectTransform rectTransform = GetComponent<RectTransform>();
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);

        RectTransform rt = this.gameObject.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(width, 20);

        //places the item on the timeline
        rt.transform.localPosition = new Vector3(x, 0);

        /*float start = (startDate.year - timelineStart) * 7.5f;
        float end = (endDate.year - startDate.year) * 7.5f;


        rectTransform.localPosition = new Vector3(start - 750, 0);
        rectTransform.sizeDelta = new Vector2(end, rectTransform.sizeDelta.y);*/
    }


}
