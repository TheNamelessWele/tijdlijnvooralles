using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PlaceInSlot : MonoBehaviour
{
    //goes through every single one of the timelineslots 
    //checks whether there's already an item in that gap on the timeslot: if yes, go to next timeslot 

    static public GameObject slotContainer;
    public HashSet<Collider> colliders = new HashSet<Collider>();
    public bool collidesWithObject;

    public void OnURLClick()
    {
        Debug.Log("Entered URLClick");
        string hyperlink = this.gameObject.transform.GetChild(6).GetComponent<TMPro.TMP_Text>().text;
        Debug.Log("Link: " + hyperlink);
        Application.OpenURL(hyperlink);
        Debug.LogError("Opened URL!");
    }


}
