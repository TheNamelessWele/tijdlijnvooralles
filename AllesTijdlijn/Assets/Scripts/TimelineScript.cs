using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Text.RegularExpressions;
using System.IO;
using System.Globalization;
using UnityEngine.Networking;

public class TimelineScript : MonoBehaviour
{
    //ToDo: 
    //Every unique tag needs to be saved in a List, if tag != any other tag
    //Per event in the gamedata txt, a new Item needs to be added, with the name, startdate, enddate, tag, priority, booleans StartDateSure and EndDateSure
    //new events need to be added to the gamedata.txt
    //Check if new event name is the same as another name in the txt, or has the same start and enddate: prompt user "are you sure you don't mean..."

    //puts start and end dates at the ends of each timeline
    public InputField beginningDate;
    public InputField endingDate;
    public static int tBeginDate;
    public static int tEndDate;

    public Date startDate;
    public Date endDate;
    public GameObject slotContainer;
    public GameObject secondContainer;
    public GameObject item;
    public string info;


    public bool isEventAdd = false;

    public static List<Item> events;
    public static HashSet<Item> itemObjects;
    public GameObject prefab;
    public GameObject shortPrefab;
    public GameObject eventItem;
    public GameObject prefabTag;
    public GameObject prefabCard;
    public FileStream fileStream;
    public StreamReader inputStream;
    public DragDrop dragDrop;
    public PlaceInSlot placeInSlot;

    WriteToFile writeToFile;

    public const int NAME = 1;
    public const int SDATE = 2;
    public const int EDATE = 3;
    public const int TAGS = 4;
    public const int INFO = 5;
    public const int COUNTRY = 6;
    public const int SDATESURE = 7;
    public const int EDATESURE = 8;
    public const int LINK = 9;
    public const int LOOPLIMIT_MULT = 1000;


    public void Start()
    {
        events = new List<Item>();
        itemObjects = new HashSet<Item>();
        bool isWebResource = false; //set true when actually porting to WebGL 
        if (!isEventAdd)
        {
            if (isWebResource)
            {
                //load asset using streamingassets and unity web request 
                UnityWebRequest loadingRequest = UnityWebRequest.Get("http://xander.stuff.ovh:8080/tijdlijn/gamedata.tsv");
                
                Debug.Log("Loaded!");

                StartCoroutine(StartReadWeb(loadingRequest));
            }
            else //when files are on computer: PC version
            {

                //check for custom beginning and end dates
                int cBeginDate;
                int cEndDate;
                string sBeginningDate = beginningDate.text;
                string sEndingDate = endingDate.text;
                bool customStart = int.TryParse(sBeginningDate, out cBeginDate);
                bool customEnd = int.TryParse(sEndingDate, out cEndDate);
                Debug.LogError(sBeginningDate + " - " + sEndingDate + " Start can parse to int: " + customStart + " End can parse to int: " + customEnd);

                if (!customStart && customEnd)
                {
                    tBeginDate = cEndDate - 200;
                    tEndDate = cEndDate;
                    Debug.LogError("Default beginning date, custom end");
                }
                else if (customStart && !customEnd)
                {
                    tBeginDate = cBeginDate;
                    tEndDate = cBeginDate + 200;
                    Debug.LogError("Custom beginning date, default end");
                }
                else if (customStart && customEnd)
                {
                    tBeginDate = cBeginDate;
                    tEndDate = cEndDate;
                }
                else
                {
                    tBeginDate = 1800;
                    tEndDate = 2000;
                    Debug.LogError("Default beginning date, default end");
                } 

                //reads data out
                var file = Resources.Load<TextAsset>(Application.streamingAssetsPath + "/gamedata.tsv");
                fileStream = new FileStream(Application.streamingAssetsPath + "/gamedata.tsv", FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.None);
                inputStream = new StreamReader(fileStream);                
                StartCoroutine(StartReadFile());
            }                        
        } 
    }

    public void PlaceEvents()
    {
        Color lightred      = new Color(222 / 255f, 67 / 255f, 67 / 255f);
        Color orange        = new Color(255 / 255f, 108 / 255f, 47 / 255f);
        Color gold          = new Color(255 / 255f, 181 / 255f, 17 / 255f);
        Color lightgreen    = new Color(112 / 255f, 171 / 255f, 55 / 255f);
        Color darkgreen     = new Color(24 / 255f, 145 / 255f, 89 / 255f);
        Color lightblue     = new Color(122 / 255f, 177 / 255f, 232 / 255f);
        Color darkblue      = new Color(1 / 255f, 110 / 255f, 254 / 255f);
        Color purple        = new Color(89 / 255f, 83 / 255f, 126 / 255f);
        Color lightgrey     = new Color(77 / 255f, 77 / 255f, 77 / 255f);     

        //how many days there are in the years, to be able to place the items correctly. Takes leap years into account
        int leapYears = LeapYearsBetween(tBeginDate, tEndDate);
        int leapDays = leapYears * 366;
        int restYears = (tEndDate - tBeginDate) - leapYears;
        int restdays = restYears * 365;

        int days = leapDays + restdays;

        int loops = 0;
        float pixelsPerDay = 1500.00f / days;


        //instantiates items in timeline
        foreach (var item in events)
        {
            if(item.startDate.year <= item.endDate.year )
            {
                //instantiates new items
                GameObject newItem;
                if(item.startDate.year < item.endDate.year && item.endDate.year - item.startDate.year >= 10)
                {
                    newItem = Instantiate(prefab);
                }
                else if(item.startDate.year == item.endDate.year)
                {
                    newItem = Instantiate(eventItem);
                }
                else
                {
                    newItem = Instantiate(shortPrefab);
                }

                Item currentItem = newItem.GetComponent<Item>();
                currentItem.name = item.objName;
                currentItem.startDate = item.startDate;
                currentItem.endDate = item.endDate;
                currentItem.tags = item.tags;

                GameObject childColour2 = newItem.transform.GetChild(0).gameObject; //tertiary card tag colour
                GameObject childColour1 = newItem.transform.GetChild(1).gameObject; //secondary card tag colour
                GameObject childSDate = newItem.transform.GetChild(2).gameObject; //startdate card
                GameObject childEDate = newItem.transform.GetChild(3).gameObject; //enddate card
                GameObject childColour = newItem.transform.GetChild(4).gameObject; //primary card tag colour
                GameObject childItemName = newItem.transform.GetChild(5).gameObject; //name card
                GameObject childCard = newItem.transform.GetChild(6).gameObject; //card of item, that contains all info

                //create card 
                //card outline 
                //card contents
                GameObject ichildCard = childCard.transform.GetChild(2).gameObject;
                ichildCard.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = currentItem.name;
                ichildCard.transform.GetChild(1).GetComponent<TMPro.TMP_Text>().text = currentItem.startDate.day + "/" + currentItem.startDate.month + "/" + currentItem.startDate.year;
                ichildCard.transform.GetChild(2).GetComponent<TMPro.TMP_Text>().text = currentItem.endDate.day + "/" + currentItem.endDate.month + "/" + currentItem.endDate.year;
                ichildCard.transform.GetChild(5).GetComponent<TMPro.TMP_Text>().text = item.information.ToString();
                ichildCard.transform.GetChild(6).GetComponent<TMPro.TMP_Text>().text = item.link.ToString();

                GameObject tagContainer = ichildCard.transform.GetChild(8).gameObject;

                //tags
                int tagIndex = 0;
                GameObject newTag;

                foreach (string tag in currentItem.tags)
                {
                    newTag = tagContainer.transform.GetChild(tagIndex).gameObject;
                    newTag.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text = tag;
                    newTag.SetActive(true);
                    if (tagIndex < 3)
                    {
                        tagIndex++;
                    }
                    else tagIndex = 0;
                    
                    switch (tag)
                    {
                        case "music":
                            newTag.transform.GetComponent<Image>().color = lightred;
                            break;
                        case "literature":
                            newTag.transform.GetComponent<Image>().color = lightred;
                            break;
                        case "economy":
                            newTag.transform.GetComponent<Image>().color = orange;
                            break;
                        case "science":
                            newTag.transform.GetComponent<Image>().color = orange;
                            break;
                        case "people":
                            newTag.transform.GetComponent<Image>().color = gold;
                            break;
                        case "philosophy":
                            newTag.transform.GetComponent<Image>().color = gold;
                            break;
                        case "politics":
                            newTag.transform.GetComponent<Image>().color = lightgreen;
                            break;
                        case "religion":
                            newTag.transform.GetComponent<Image>().color = lightgreen;
                            break;
                        case "visual arts":
                            newTag.transform.GetComponent<Image>().color = darkgreen;
                            break;
                        case "fashion":
                            newTag.transform.GetComponent<Image>().color = darkgreen;
                            break;
                        case "exploration":
                            newTag.transform.GetComponent<Image>().color = lightblue;
                            break;
                        case "pioneer":
                            newTag.transform.GetComponent<Image>().color = lightblue;
                            break;
                        case "sports":
                            newTag.transform.GetComponent<Image>().color = darkblue;
                            break;
                        case "entertainment":
                            newTag.transform.GetComponent<Image>().color = darkblue;
                            break;
                        case "law":
                            newTag.transform.GetComponent<Image>().color = purple;
                            break;
                        case "war":
                            newTag.transform.GetComponent<Image>().color = purple;
                            break;
                        default:
                            newTag.transform.gameObject.SetActive(false);
                            break;

                    }
                }

                //places item
                int index = 0;
                do
                {
                    loops++; //loops so that there are no infinite loops trying to place an item
                    if (slotContainer.transform.GetChild(index).childCount == 0) //if there's nothing on that line
                    {
                        newItem.gameObject.transform.SetParent(slotContainer.transform.GetChild(index)); //place immediately
                        break;
                    }
                    else
                    {
                        int highestEnd = tBeginDate; //TODO: 
                        int lowestStart = tEndDate; //TODO: 

                        foreach (Transform child in slotContainer.transform.GetChild(index).transform) //to see what date something should come before or after
                        {
                            if (lowestStart > child.gameObject.GetComponent<Item>().startDate.year) { lowestStart = child.gameObject.GetComponent<Item>().startDate.year; }
                            if (highestEnd < child.gameObject.GetComponent<Item>().endDate.year) { highestEnd = child.gameObject.GetComponent<Item>().endDate.year; }
                        }

                        if (newItem.GetComponent<Item>().startDate.year < highestEnd + 10) //if it starts before the highest end
                        {
                            if (newItem.GetComponent<Item>().endDate.year < lowestStart - 10) //and before the lowest start (so comes entirely before another object)
                            {
                                newItem.gameObject.transform.SetParent(slotContainer.transform.GetChild(index)); //place item on line
                                break;
                            }
                            if (index < 127) //else, place on next line
                            {
                                index++;
                            }
                            else
                            {
                                index = 0;
                            }
                            continue; //executes the code above again
                        }
                        newItem.gameObject.transform.SetParent(slotContainer.transform.GetChild(index)); //if startdate is higher than the highest end date, place item
                        break;
                    }
                }
                while (loops < events.Count * LOOPLIMIT_MULT); //no infinite loops

                //snaps it properly
                newItem.transform.position = new Vector3(newItem.transform.position.x, newItem.transform.position.y, newItem.transform.position.z);
                newItem.GetComponent<RectTransform>().anchoredPosition = new Vector3(1, 1, 1);
                newItem.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1); //scale properly

                childSDate.GetComponent<TMPro.TMP_Text>().text = currentItem.startDate.year.ToString();
                childEDate.GetComponent<TMPro.TMP_Text>().text = currentItem.endDate.year.ToString();
                childItemName.GetComponent<TMPro.TMP_Text>().text = currentItem.name;

                //Card colours
                //primary card colour 
                switch (currentItem.tags[0])
                {
                    case "music":
                        childColour.GetComponent<Image>().color = lightred; //red
                        childColour1.GetComponent<Image>().color = lightred;
                        childColour2.GetComponent<Image>().color = lightred;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = lightred;
                        break;
                    case "literature":
                        childColour.GetComponent<Image>().color = lightred;
                        childColour1.GetComponent<Image>().color = lightred;
                        childColour2.GetComponent<Image>().color = lightred;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = lightred;
                        break;
                    case "economy":
                        childColour.GetComponent<Image>().color = orange; //orange
                        childColour1.GetComponent<Image>().color = orange;
                        childColour2.GetComponent<Image>().color = orange;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = orange;
                        break;
                    case "science":
                        childColour.GetComponent<Image>().color = orange;
                        childColour1.GetComponent<Image>().color = orange;
                        childColour2.GetComponent<Image>().color = orange;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = orange;
                        break;
                    case "people":
                        childColour.GetComponent<Image>().color = gold; //yellow
                        childColour2.GetComponent<Image>().color = gold;
                        childColour1.GetComponent<Image>().color = gold;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = gold;
                        break;
                    case "philosophy":
                        childColour.GetComponent<Image>().color = gold;
                        childColour2.GetComponent<Image>().color = gold;
                        childColour1.GetComponent<Image>().color = gold;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = gold;
                        break;
                    case "politics":
                        childColour.GetComponent<Image>().color = lightgreen; //green
                        childColour2.GetComponent<Image>().color = lightgreen;
                        childColour1.GetComponent<Image>().color = lightgreen;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = lightgreen;
                        break;
                    case "religion":
                        childColour.GetComponent<Image>().color = lightgreen;
                        childColour2.GetComponent<Image>().color = lightgreen;
                        childColour1.GetComponent<Image>().color = lightgreen;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = lightgreen;
                        break;
                    case "visual arts":
                        childColour.GetComponent<Image>().color = darkgreen; //dark green
                        childColour2.GetComponent<Image>().color = darkgreen;
                        childColour1.GetComponent<Image>().color = darkgreen;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = darkgreen;
                        break;
                    case "fashion":
                        childColour.GetComponent<Image>().color = darkgreen;
                        childColour2.GetComponent<Image>().color = darkgreen;
                        childColour1.GetComponent<Image>().color = darkgreen;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = darkgreen;
                        break;
                    case "exploration":
                        childColour.GetComponent<Image>().color = lightblue; //light blue
                        childColour2.GetComponent<Image>().color = lightblue;
                        childColour1.GetComponent<Image>().color = lightblue;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = lightblue;
                        break;
                    case "pioneer":
                        childColour.GetComponent<Image>().color = lightblue;
                        childColour2.GetComponent<Image>().color = lightblue;
                        childColour1.GetComponent<Image>().color = lightblue;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = lightblue;
                        break;
                    case "sports":
                        childColour.GetComponent<Image>().color = darkblue; //dark blue
                        childColour2.GetComponent<Image>().color = darkblue;
                        childColour1.GetComponent<Image>().color = darkblue;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = darkblue;
                        break;
                    case "entertainment":
                        childColour.GetComponent<Image>().color = darkblue;
                        childColour2.GetComponent<Image>().color = darkblue;
                        childColour1.GetComponent<Image>().color = darkblue;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = darkblue;
                        break;
                    case "law":
                        childColour.GetComponent<Image>().color = purple; //purple
                        childColour2.GetComponent<Image>().color = purple;
                        childColour1.GetComponent<Image>().color = purple;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = purple;
                        break;
                    case "war":
                        childColour.GetComponent<Image>().color = purple;
                        childColour2.GetComponent<Image>().color = purple;
                        childColour1.GetComponent<Image>().color = purple;
                        childCard.transform.GetChild(0).GetComponent<Image>().color = purple;
                        break;
                    default:
                        childColour.GetComponent<Image>().color = Color.grey;
                        childColour2.GetComponent<Image>().color = Color.grey;
                        childColour2.GetComponent<Image>().color = Color.grey;
                        childCard.transform.GetChild(0).gameObject.SetActive(false);
                        break;
                }
                
                //secondary card colour
                if (currentItem.tags.Count >= 2)
                {
                    childColour1.gameObject.SetActive(true);
                    switch (currentItem.tags[1])
                    {
                        case "music": childColour1.GetComponent<Image>().color = lightred; childColour2.GetComponent<Image>().color = lightred; break;
                        case "literature": childColour1.GetComponent<Image>().color = lightred; childColour2.GetComponent<Image>().color = lightred; break;
                        case "economy": childColour1.GetComponent<Image>().color = orange; childColour2.GetComponent<Image>().color = orange; break;
                        case "science": childColour1.GetComponent<Image>().color = orange; childColour2.GetComponent<Image>().color = orange; break;
                        case "people": childColour1.GetComponent<Image>().color = gold; childColour2.GetComponent<Image>().color = gold; break;
                        case "philosophy": childColour1.GetComponent<Image>().color = gold; childColour2.GetComponent<Image>().color = gold; break;
                        case "politics": childColour1.GetComponent<Image>().color = lightgreen; childColour2.GetComponent<Image>().color = lightgreen; break;
                        case "religion": childColour1.GetComponent<Image>().color = lightgreen; childColour2.GetComponent<Image>().color = lightgreen; break;
                        case "visual arts": childColour1.GetComponent<Image>().color = darkgreen; childColour2.GetComponent<Image>().color = darkgreen; break;
                        case "fashion": childColour1.GetComponent<Image>().color = darkgreen; childColour2.GetComponent<Image>().color = darkgreen; break;
                        case "exploration": childColour1.GetComponent<Image>().color = lightblue; childColour2.GetComponent<Image>().color = lightblue; break;
                        case "pioneer": childColour1.GetComponent<Image>().color = lightblue; childColour2.GetComponent<Image>().color = lightblue; break;
                        case "sports": childColour1.GetComponent<Image>().color = darkblue; childColour2.GetComponent<Image>().color = darkblue; break;
                        case "entertainment": childColour1.GetComponent<Image>().color = darkblue; childColour2.GetComponent<Image>().color = darkblue; break;
                        case "law": childColour1.GetComponent<Image>().color = purple; childColour2.GetComponent<Image>().color = purple; break;
                        case "war": childColour1.GetComponent<Image>().color = purple; childColour2.GetComponent<Image>().color = purple; break;
                        default: childColour1.GetComponent<Image>().color = Color.grey; childColour2.GetComponent<Image>().color = Color.grey; break;

                    }
                }

                //tertiary card colour
                if (currentItem.tags.Count >= 3)
                {
                    childColour2.gameObject.SetActive(true);
                    switch (currentItem.tags[2])
                    {
                        case "music": childColour2.GetComponent<Image>().color = lightred; break;
                        case "literature": childColour2.GetComponent<Image>().color = lightred; break;
                        case "economy": childColour2.GetComponent<Image>().color = orange; break;
                        case "science": childColour2.GetComponent<Image>().color = orange; break;
                        case "people": childColour2.GetComponent<Image>().color = gold; break;
                        case "philosophy": childColour2.GetComponent<Image>().color = gold; break;
                        case "politics": childColour2.GetComponent<Image>().color = lightgreen; break;
                        case "religion": childColour2.GetComponent<Image>().color = lightgreen; break;
                        case "visual arts": childColour2.GetComponent<Image>().color = darkgreen; break;
                        case "fashion": childColour2.GetComponent<Image>().color = darkgreen; break;
                        case "exploration": childColour2.GetComponent<Image>().color = lightblue; break;
                        case "pioneer": childColour2.GetComponent<Image>().color = lightblue; break;
                        case "sports": childColour2.GetComponent<Image>().color = darkblue; break;
                        case "entertainment": childColour2.GetComponent<Image>().color = darkblue; break;
                        case "law": childColour2.GetComponent<Image>().color = purple; break;
                        case "war": childColour2.GetComponent<Image>().color = purple; break;
                        default: childColour2.GetComponent<Image>().color = Color.grey; break;

                    }
                }

                //placing everything in the correct place on the timeline horizontally

                int itemYears = currentItem.endDate.year - currentItem.startDate.year;
                int itemMonths = currentItem.endDate.month - currentItem.startDate.month;
                int itemDays = currentItem.endDate.day - currentItem.startDate.day;

                int width = Mathf.FloorToInt((itemYears * 365 + itemMonths * 30 + itemDays) * pixelsPerDay);

                itemYears = currentItem.startDate.year - tBeginDate - 100;
                itemMonths = currentItem.startDate.month - 1;
                itemDays = currentItem.startDate.day - 1;

                int itemX = Mathf.FloorToInt((itemYears * 365 + itemMonths * 30 + itemDays) * pixelsPerDay);

                //scales the item to match the date
                if (width < 25)
                {
                    width = 25; //minimum width
                }
                currentItem.width = width;
                currentItem.x = itemX;

                RectTransform rt = newItem.GetComponent<RectTransform>();
                rt.sizeDelta = new Vector2(width, 20);

                //places the item on the timeline
                rt.transform.localPosition = new Vector3(itemX, 0);
                //add to the hashset used for filtering
                itemObjects.Add(currentItem);
            }

            
        }
    }

    public void PlaceOnTimeline(HashSet<Item> result) //places items on timeline
    {
        int index = 0;
        foreach (var item in result)
        {
            if(item.transform.parent.parent.name != "SlotCollection")
            {
                continue;
            }
            item.gameObject.transform.SetParent(slotContainer.transform.GetChild(index));
            index++;
        }
    }

    public IEnumerator StartReadFile() //starts reading the file in a new thread
    {
        //Debug.Log("Started");
        yield return StartCoroutine(ReadFile());
        //Debug.Log("Coroutine started");
        StopCoroutine(ReadFile());
        //Debug.Log("Coroutine stopped");
        PlaceEvents();
        //Debug.Log("Placed Events");
    }

    public IEnumerator StartReadWeb(UnityWebRequest loadingRequest) //to read files from web, for WebGL 
    {
        yield return loadingRequest.SendWebRequest();
        Debug.Log("Started Reading Web");
        yield return StartCoroutine(WaitUntilResolve(loadingRequest));
    }
    
    public void Snap(GameObject newItem) //snaps items to the right place in the timeline
    {
        int index = 0;
        int startDate = int.Parse(newItem.gameObject.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text);
        int endDate = int.Parse(newItem.gameObject.transform.GetChild(1).GetComponent<TMPro.TMP_Text>().text);
        GameObject slot = new GameObject();
        slot = slotContainer.transform.GetChild(index).gameObject;
       
        if (slotContainer.transform.GetChild(index).childCount == 1)
        {
            newItem.gameObject.transform.SetParent(slotContainer.transform.GetChild(index));
        }
        else if(slot.transform.GetChild(index).childCount > 1)
        {
            int childStart = slot.transform.GetChild(1).GetComponent<Item>().startDate.year;
            int childEnd = slot.transform.GetChild(1).GetComponent<Item>().endDate.year;
                if (startDate < childEnd || endDate > childStart)
                {
                    Debug.Log("overlaps");
                    if (index < 128)
                    {
                        index++;
                    }
                }
                else 
                {
                    newItem.gameObject.transform.SetParent(slotContainer.transform.GetChild(index));

                }
        }
    }

    public void ReloadTimeline() //empties and reloads the events in the timeline, for when the dates change
    {
        foreach (Transform item in slotContainer.transform)
        {
            foreach (Transform child in item.transform)
            {
                Destroy(child.gameObject);
            }
        }
        Start();
    }

    public void CloseAllCards() //closes all cards
    {
        foreach(Transform child in secondContainer.transform)
        {
            child.gameObject.SetActive(false);
        }
    }

    public static int LeapYearsBetween(int start, int end) //calculates leapyears between two years
    {
        return LeapYearsBefore(end) - LeapYearsBefore(start + 1);
    }

    static int LeapYearsBefore(int year) //calculates leap years before years
    {
        year--;
        return (year / 4) - (year / 100) + (year / 400);
    }

    public IEnumerator ReadFile() //reads fileIO
    {
        inputStream.ReadLine();
        string line;
        do
        {

            line = inputStream.ReadLine();
            if (line != null)
            {
                LineToEvent(line);
            }
        } while (line != null);
        inputStream.Close();
        yield return null;
    }

    public void LineToEvent(string line) //turns read lines from file IO into events via regex
    {
        //Debug.Log(line + "");
        Match match = Regex.Match(line, @"^(.*?)\t(\d+\/\d+\/-?\d+)\t(\d+\/\d+\/-?\d+)\t(.*?)\t(.*?)\t(.*?)\t(.*?)\t(.*?)\t(.*)$"); //to decipher, check regex101.com
                                                                                                                                    //Debug.Log(match.Value);
        Match sDate = Regex.Match(match.Groups[SDATE].Value, @"(\d+)\/(\d+)\/(\-?\d+)");
        Match eDate = Regex.Match(match.Groups[EDATE].Value, @"(\d+)\/(\d+)\/(\-?\d+)");

        int.TryParse(sDate.Groups[1].Value, out int startDay);
        int.TryParse(sDate.Groups[2].Value, out int startMonth);
        int.TryParse(sDate.Groups[3].Value, out int startYear);
        Date startDate = new Date(startDay, startMonth, startYear);

        int.TryParse(eDate.Groups[1].Value, out int endDay);
        int.TryParse(eDate.Groups[2].Value, out int endMonth);
        int.TryParse(eDate.Groups[3].Value, out int endYear);
        Date endDate = new Date(endDay, endMonth, endYear);

        //initialises tBeginDate and tEndDate
        /*if (!int.TryParse(beginningDate.text, out tBeginDate) && int.TryParse(endingDate.text, out tEndDate))
        {
            tBeginDate = int.Parse(beginningDate.text);
            tEndDate = int.Parse(endingDate.text);
            Debug.LogError("Default beginning date, custom end");
        }
        else if (int.TryParse(beginningDate.text, out tBeginDate) && !int.TryParse(endingDate.text, out tEndDate))
        {
            tBeginDate = int.Parse(beginningDate.text);
            tEndDate = 2000;
            Debug.LogError("Custom beginning date, default end");
        }
        else
        {
            tBeginDate = 1800;
            tEndDate = 2000;
            Debug.LogError("Default beginning date, default end");
        }*/

        if (startDate.year < tBeginDate || endDate.year > tEndDate)
        {
            return; //continue skips rest of loop and goes back to beginning
        }

        string[] tagNames = match.Groups[TAGS].Value.Split(',');
        string[] country = match.Groups[COUNTRY].Value.Split(',');
        bool startDateSure = match.Groups[SDATESURE].Value.ToLower() == "yes";
        bool endDateSure = match.Groups[EDATESURE].Value.ToLower() == "yes";
        string link = match.Groups[LINK].Value.ToLower();


        for (int i = 0; i < tagNames.Length; i++)
        {
            tagNames[i] = tagNames[i].Trim().ToLower();
        }

        List<string> tags = new List<string>();

        //Debug.Log(match.Groups[2].Value);
        //Debug.Log(match.Groups[3].Value);

        CultureInfo provider = CultureInfo.InvariantCulture;



        info = match.Groups[INFO].ToString();


        events.Add(new Item(match.Groups[NAME].Value.ToString(), //item name
            startDate, //item startdate
            match.Groups[EDATE].Value == "now"
                ? new Date(int.Parse(System.DateTime.Now.Day.ToString()),
                    int.Parse(System.DateTime.Now.Month.ToString()),
                    int.Parse(System.DateTime.Now.Year.ToString()))
                : endDate, //item enddate
            new List<string>(tagNames),
            info,
            new List<string>(country),
            startDateSure,
            endDateSure,
            link
        )); //split tags
        foreach (var item in events)
        {
            foreach (var tag in item.tags)
            {
                tag.Replace(" ", "");
            }
        }
    }

    public IEnumerator WaitUntilResolve(UnityWebRequest webRequest) //webGL waits until the webrequest has resolved
    {
        yield return webRequest;
        if (string.IsNullOrEmpty(webRequest.error))
        {
            Debug.Log("Waiting until resolved");
            ReadFromWeb(webRequest);
        }
    }

    public void ReadFromWeb(UnityWebRequest webRequest) //reads info file from online database
    {
        Debug.Log("Reads from web");
        string readText = webRequest.downloadHandler.text;
        Debug.Log("Data read: " + readText);
        string[] webText = webRequest.downloadHandler.text.Split('\n');
        Debug.Log(webText.Length);

        for (int i = 1; i < webText.Length; i++)
        {
            LineToEvent(webText[i] + "");
        }
        PlaceEvents();
    }
}
