using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tag
{
    public string Name { get; }
    public Color Color { get; }
    public HashSet<Item> Objects { get; }

    public Tag(string name, Color color)
    {
        Name = name;
        Color = color;
        Objects = new HashSet<Item>();
    }
}
