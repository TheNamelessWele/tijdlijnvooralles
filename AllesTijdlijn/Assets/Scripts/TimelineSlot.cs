using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TimelineSlot : MonoBehaviour, IDropHandler
{
    public const int LOOPLIMIT_MULT = 1000;
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            Debug.Log("DROP SUCCESS");
            eventData.pointerDrag.GetComponent<DragDrop>().isSuccesfuldDrop = true;
            eventData.pointerDrag.gameObject.transform.parent = this.transform;
            GameObject movedItem = eventData.pointerDrag.gameObject;
            eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;
            eventData.pointerDrag.GetComponent<Item>().SetObject();

            int highestEnd = TimelineScript.tBeginDate; //TODO: 
            int lowestStart = TimelineScript.tEndDate; //TODO: 

            int leapYears = TimelineScript.LeapYearsBetween(TimelineScript.tBeginDate, TimelineScript.tEndDate); //replace numbers with start and enddate
            int leapDays = leapYears * 366;
            int restYears = (TimelineScript.tEndDate - TimelineScript.tBeginDate) - leapYears;
            int restdays = restYears * 365;

            int days = leapDays + restdays;
            float pixelsPerDay = 1500.00f / days;


            GameObject currentSlot = this.gameObject;
            if (currentSlot.transform.childCount > 1)
            {
                List<Transform> childList = new List<Transform>();
                foreach(Transform itemchild in this.gameObject.transform)
                {
                    childList.Add(itemchild);
                }
                foreach (Transform child in childList)
                {
                    if (child.gameObject != movedItem.gameObject)
                    {
                        if (child.gameObject.GetComponent<Item>().startDate.year < movedItem.gameObject.GetComponent<Item>().endDate.year + 10)
                        {
                            if (child.gameObject.GetComponent<Item>().endDate.year > movedItem.gameObject.GetComponent<Item>().startDate.year - 10)
                            {
                                int loops = 0;
                                int index = 0;
                                //move child to other slot 
                                do
                                {
                                    loops++;

                                    foreach (Transform child2 in currentSlot.transform.parent.transform.GetChild(index).transform) //to see what date something should come before or after
                                    {
                                        if (lowestStart > child2.gameObject.GetComponent<Item>().startDate.year) { lowestStart = child2.gameObject.GetComponent<Item>().startDate.year; }
                                        if (highestEnd < child2.gameObject.GetComponent<Item>().endDate.year) { highestEnd = child2.gameObject.GetComponent<Item>().endDate.year; }
                                    }

                                    if(currentSlot.transform.parent.transform.GetChild(index).transform.childCount == 0)
                                    {
                                        child.gameObject.transform.SetParent(currentSlot.transform.parent.transform.GetChild(index)); //place item on line
                                        break;
                                    }
                                    else if (child.gameObject.GetComponent<Item>().startDate.year < highestEnd + 10) //if it starts before the highest end
                                    {
                                        if (child.gameObject.GetComponent<Item>().endDate.year < lowestStart - 10) //and before the lowest start (so comes entirely before another object)
                                        {
                                            child.gameObject.transform.SetParent(currentSlot.transform.parent.transform.GetChild(index)); //place item on line
                                            break;
                                        }
                                        if (index < 127) //else, place on next line
                                        {
                                            index++;
                                        }
                                        else
                                        {
                                            index = 0;
                                        }
                                        continue; //executes the code above again
                                    }
                                }
                                while (loops < LOOPLIMIT_MULT);


                                child.transform.position = new Vector3(child.transform.position.x, child.transform.position.y, child.transform.position.z);
                                child.GetComponent<RectTransform>().anchoredPosition = new Vector3(1, 1, 1);
                                child.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1); //scale properly

                                int itemYears = child.GetComponent<Item>().endDate.year - child.GetComponent<Item>().startDate.year;
                                int itemMonths = child.GetComponent<Item>().endDate.month - child.GetComponent<Item>().startDate.month;
                                int itemDays = child.GetComponent<Item>().endDate.day - child.GetComponent<Item>().startDate.day;

                                int width = Mathf.FloorToInt((itemYears * 365 + itemMonths * 30 + itemDays) * pixelsPerDay);

                                itemYears = child.GetComponent<Item>().startDate.year - TimelineScript.tBeginDate - 100;
                                itemMonths = child.GetComponent<Item>().startDate.month - 1;
                                itemDays = child.GetComponent<Item>().startDate.day - 1;

                                int itemX = Mathf.FloorToInt((itemYears * 365 + itemMonths * 30 + itemDays) * pixelsPerDay);

                                RectTransform rt = child.GetComponent<RectTransform>();
                                rt.sizeDelta = new Vector2(width, 20);

                                //places the item on the timeline
                                rt.transform.localPosition = new Vector3(itemX, 0);
                            }
                        }

                    }
                }
            }

        }
    }
}
