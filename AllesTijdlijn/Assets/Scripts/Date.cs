using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Date
{
    public int day;
    public int month;
    public int year; 

    public Date(int day, int month, int year)
    {
        this.day = day;
        this.month = month;
        this.year = year;
    }
}
