using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.IO;
using UnityEngine.UI;
using UnityEngine.Networking;

public class WriteToFile : MonoBehaviour
{
    //input form has:
    //name event 
    //checkbox Has Stardate 
    //startdate 
    //checkbox Has Enddate 
    //enddate 
    //Tag field which will contain the tags dragged into there 
    //info field 

    //on click AddItem, add new item to list 'newEvents' 
    //on click export items, export all items in 'newEvents' to the .txt file 
    //foreach item in list 'newEvents', add item in list 'events'
    //rerun script that places events in scene

    public List<Item> newEvents;

    //user input fields
    public TMPro.TMP_InputField inputNameEvent;
    public TMPro.TMP_InputField inputStartDate;
    public TMPro.TMP_InputField inputEndDate;
    public TMPro.TMP_InputField inputInfo;
    public GameObject startCheck;
    public GameObject endCheck;
    public TMPro.TMP_InputField inputLink;
    public TMPro.TMP_InputField inputCountry;

    //container containing tags that the user drags in
    public GameObject tagContainer;
    public GameObject tagsToChoose;

    public GameObject exceptionPrefab;
    public GameObject popupPrefab;

    //the values that are going to be entered into List<Item> newEvents


    //converting

    public TimelineScript timeline;
    FileStream fileStream;

    public void Start()
    {

    }

    public void CreateItem()
    {
        string nameEvent;
        List<string> tags = new List<string>();
        string info;
        List<string> country = new List<string>();
        bool startDateSure; //TODO: read from UI
        bool endDateSure;
        string link;

        Date startDate;
        Date endDate;
        string[] startDateArr = new string[3];
        string[] endDateArr = new string[3];

        if(inputNameEvent.text != "" && inputInfo.text != "" && inputStartDate.text != "" && inputEndDate.text != "" && inputLink.text != "" && inputCountry.text != "" 
            && tagContainer.transform.childCount != 0)
        {
            nameEvent = inputNameEvent.text;
            info = inputInfo.text;
            startDateArr = (inputStartDate.text.Split('/'));
            endDateArr = inputEndDate.text.Split('/');
            startDateSure = startCheck.GetComponent<Toggle>().isOn;
            endDateSure = endCheck.GetComponent<Toggle>().isOn;
            link = inputLink.text;
            country = new List<string>(inputCountry.text.Split(','));

            int day;
            int month;
            int year;

            int.TryParse(startDateArr[0], out day);
            int.TryParse(startDateArr[1], out month);
            int.TryParse(startDateArr[2], out year);

            startDate = new Date(day, month, year);

            int.TryParse(endDateArr[0], out day);
            int.TryParse(endDateArr[1], out month);
            int.TryParse(endDateArr[2], out year);

            endDate = new Date(day, month, year);

            foreach (Transform child in tagContainer.transform) //add all text in children of TagContainer to List tags
            {
                tags.Add(child.transform.GetChild(0).GetComponent<TMPro.TMP_Text>().text.ToLower());
            }
            Item item = new Item(nameEvent, startDate, endDate, tags, info, country, startDateSure, endDateSure, link);
            GameObject popup = Instantiate(popupPrefab);
            popup.transform.parent = this.gameObject.transform.parent.transform;
            newEvents.Add(item);
            Destroy(popup, 2f);
        }
        else
        {
            GameObject popup = Instantiate(exceptionPrefab);
            popup.transform.parent = this.gameObject.transform.parent.transform;
            Destroy(popup, 2f);
        }
        
    }

    public void ClearFields()
    {
        inputNameEvent.Select();
        inputNameEvent.text = "";
        inputStartDate.Select();
        inputStartDate.text = "";
        inputEndDate.Select();
        inputEndDate.text = "";
        inputInfo.Select();
        inputInfo.text = "";
        startCheck.SetActive(true);
        endCheck.SetActive(true);
        inputLink.Select();
        inputLink.text = "";
        inputCountry.Select();
        inputCountry.text = "";

        int children = tagContainer.transform.childCount;
        for (int i = 0; i < 16; i++)
        {
            if(tagContainer.transform.GetChild(0) != null)
            {
                tagContainer.transform.GetChild(0).SetParent(tagsToChoose.transform);
            }     
            Debug.Log("Cleared tag");
        }
        Debug.Log("Cleared fields!");
    }

    public void ExportItems() //can apparently not have a streamwriter and a streamreader in seperate streams.
    {
        /*var loadingRequest = UnityWebRequest.Get(Path.Combine(Application.streamingAssetsPath, "/gamedata.tsv"));
        while (!loadingRequest.isDone)
        {
            if (loadingRequest.isNetworkError || loadingRequest.isHttpError) { break; }
        }
        if (loadingRequest.isNetworkError || loadingRequest.isHttpError) { }
        else
        {
            File.WriteAllBytes(Path.Combine(Application.streamingAssetsPath, "/gamedata.tsv"), loadingRequest.downloadHandler.data);
        }*/

        using (fileStream = new FileStream(Application.streamingAssetsPath + "/gamedata.tsv", FileMode.Append, FileAccess.Write, FileShare.None))
        {
            using (StreamWriter outputStream = new StreamWriter(fileStream))
            {
                foreach (Item item in newEvents)
                {
                    TimelineScript.events.Add(item);
                    string tagoutput = "";
                    foreach (var t in item.tags)
                    {
                        tagoutput += t + ',';
                    }
                    string outputVal = "\n" + item.objName + "\t" + inputStartDate.text +
                        "\t" + inputEndDate.text + "\t" + tagoutput + "\t" + item.information +
                        "\t" + inputCountry.text + "\t" + (item.startDateSure ? "yes" : "no") + "\t" + (item.endDateSure ? "yes" : "no") + "\t" + inputLink.text;

                    outputStream.Write(outputVal);
                }
                outputStream.Close();
                Debug.Log("Closed outputstream");
            }
            //fileStream.Close();
            //Debug.Log("Closed filestream");

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
