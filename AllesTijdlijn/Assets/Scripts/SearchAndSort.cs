using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SearchAndSort : MonoBehaviour
{
    private Image background;
    private RectTransform searchMenu;
    public GameObject slotContainer;
    public GameObject slotContainer1;
    public TimelineScript timelineScript;
    public static HashSet<string> selectedTags;
    public Transform tagField;

    private bool andEnabled = false;

    // Start is called before the first frame update
    void Start()
    {
        background = GetComponent<Image>();
        searchMenu = GetComponent<RectTransform>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    HashSet<Item> Filter(bool andEnabled)
    {
        HashSet<Item> selection = new HashSet<Item>();

        //choose between AND and OR search
        if (andEnabled)
        {
            //AND SEARCH
            //use bool to determine if it's the first iteration of the foreach loop
            //bool isFirstLoop = true;

            //loop through each tag selected in the menu
            //foreach (string tag in selectedTags)
            //{
            //    //if its the first loop, fill the list with items that have the current tag
            //    if (isFirstLoop)
            //    {
            //        Debug.Log("First loop");
            //        foreach (Item item in TimelineScript.itemObjects)
            //        {
            //            if (item.tags.Contains(tag.ToLower())) selection.Add(item);
            //        }
            //        Debug.Log(selection.Count);
            //    }
            //    //if its not the first loop, remove items from the selection that don't have the current tag
            //    else
            //    {
            //        Debug.Log("Other loops");
            //        foreach (Item item in selection)
            //        {
            //            if (!item.tags.Contains(tag.ToLower())) selection.Remove(item);
            //        }
            //    }
            //    isFirstLoop = false;
            //}
            HashSet<Item> itemsToRemove = new HashSet<Item>();
            foreach (string tag in selectedTags)
            {
                foreach (Item item in TimelineScript.itemObjects)
                {
                    if (!item.tags.Contains(tag.ToLower())) itemsToRemove.Add(item);
                }
            }
            selection = new HashSet<Item>(TimelineScript.itemObjects);
            selection.ExceptWith(itemsToRemove);
        }
        else
        {
            //OR SEARCH
            //loop through each tag selected in the menu
            //TODO: can be optimized further. Find a way to avoid checking items multiple times (by creating a list of all the remaining item for example)
            foreach (string tag in selectedTags)
            {
                //Debug.Log(tag);
                //loop through all loaded items
                foreach (Item item in TimelineScript.itemObjects)
                {
                    item.gameObject.SetActive(false);
                    Debug.Log(item.tags);
                    //if the item contains the current tag, add it to the hashSet (item will not be added if it's already present)
                    if (item.tags.Contains(tag.ToLower()))
                    {
                        bool isInSet = selection.Add(item);
                        if (!isInSet)
                        {
                            Debug.Log("Adding the object");
                            item.gameObject.SetActive(true);
                        }
                        else Debug.Log("Object is already in this set");
                    }
                    else
                    {
                        Debug.Log("Tag not found");
                    }
                }
            }
        }

        return selection;
    }

    public void ShowResults()
    {
        int index = 0;
        selectedTags = new HashSet<string>();
        //get all the selected tags
        for (int i = 0; i < tagField.childCount; i++)
        {
            selectedTags.Add(tagField.GetChild(i).name);
        }
        HashSet<Item> result;
        //filter through all items
        if(selectedTags.Count == 0)
        {
            Scene scene = SceneManager.GetActiveScene();
            SceneManager.LoadScene(scene.name);
        }

        result = Filter(andEnabled);
        //debug stuff
        Debug.Log(selectedTags.Count);
        Debug.Log(TimelineScript.itemObjects.Count);
        Debug.Log(result.Count);

        //deactivate all, then reactivate selection
        foreach (Item item in TimelineScript.itemObjects)
        {
            item.gameObject.SetActive(false);
        }

        foreach (Item item in result)
        {
            RectTransform rt = item.GetComponent<RectTransform>();
            item.gameObject.SetActive(true);
            Debug.Log(item.name);
            //item.gameObject.transform.SetParent(slotContainer.transform.GetChild(index));
            timelineScript.PlaceOnTimeline(result);
            rt.transform.localPosition = new Vector3(item.x, 0, 0);
            //index++;
        }

        foreach (Transform child in slotContainer1.transform)
        {
            if(child.childCount != 0)
            {
                foreach (Transform subchild in child.transform)
                {
                    subchild.gameObject.SetActive(true);
                }
            }
        }
    }

    public void ToggleAndSearch()
    {
        andEnabled = !andEnabled;
    }
}
