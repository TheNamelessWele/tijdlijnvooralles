using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Dropable : MonoBehaviour, IDropHandler
{
    public GameObject target;

    public const int LOOPLIMIT_MULT = 1000;

    public void Start()
    {
        if (target == null)
        {
            target = this.gameObject;
        }
    }

    //This script doesn't seem to have any real value. The code under this is a duplicate of the code in TimelineSlot, and is never called, as the code in TimelineSLot is.
    //I don't know why this is here. Jannes? Why is this here? 
    public void OnDrop(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            if (eventData.pointerDrag.gameObject.transform.parent != target.transform)
            {
                Debug.Log("DROP SUCCES");
                Debug.Log(gameObject.name);
                eventData.pointerDrag.GetComponent<Dragable>().isSuccesfuldDrop = true;
                eventData.pointerDrag.gameObject.transform.SetParent(target.transform, true);
                eventData.pointerDrag.GetComponent<RectTransform>().anchoredPosition = GetComponent<RectTransform>().anchoredPosition;

                
            }
        }
    }
}
